# ASP Projekt I7

LINK ZA REPLIT
https://replit.com/@tkosak

SEGMENT 4:

Struktura 1 - Vektor
Stukrtura 2 - Forward lista

Ulazni podaci generirani su pomocu online servisa Random.org
100000 random integera u rasponu 1-10000 

Provedena mjerenja po segmentima:

NAPOMENA: Mjerenja su rađena na Replit.com što može imati utjecaj na konzistenciju rezultata mjerenja.

-----------------------------------------

Segment 2 <vector>:
Ubacuje se 10000 podataka

-----------------------------------------

Prvo mjerenje:
Ubacivanje na pocetak: 2287812 ms

Ubacivanje u sredinu: 2367365 ms

 Ubacivanje na kraj: 15732 ms
 
 Pristup podatku: 15 ms

 Brisanje pocetka: 109 ms

 Brisanje sredine: 19 ms

 Brisanje kraja: 0 ms

 ---------------------------------------

 Drugo mjerenje:
 Ubacivanje na pocetak: 1151692 ms

 Ubacivanje u sredinu: 1025300 ms

 Ubacivanje na kraj: 9271 ms

 Pristup podatku: 218 ms

 Brisanje pocetka: 104 ms

 Brisanje sredine: 148 ms

 Brisanje kraja: 1 ms

 ----------------------------------------
 Trece mjerenje:
 Ubacivanje na pocetak: 1169690 ms

 Ubacivanje u sredinu: 1166483 ms

 Ubacivanje na kraj: 11686 ms

 Pristup podatku: 11 ms

 Brisanje pocetka: 79 ms

 Brisanje sredine: 14 ms

 Brisanje kraja: 0 ms

--------------------------------------------

Segment 3 <forward_list>:
Prvo mjerenje:
Ubacivanje na pocetku: 107703 ms

 Ubacivanje na kraju: 128616 ms

 Ubacivanje na sredini: 153725 ms

 Pristup podatku: 23796 ms

 Brisanje pocetka: 244 ms

 Brisanje kraja: 27208 ms

 Brisanje sredine: 28327 ms

 ------------------------------------------

 Drugo mjerenje:
 Ubacivanje na pocetku: 18213 ms

 Ubacivanje na kraju: 34663 ms

 Ubacivanje na sredini: 19363 ms

 Pristup podatku: 865 ms

 Brisanje pocetka: 106 ms

 Brisanje kraja: 6916 ms

 Brisanje sredine: 8078 ms

 -----------------------------------------

 Trece mjerenje:
 Ubacivanje na pocetku: 29029 ms

 Ubacivanje na kraju: 96154 ms

 Ubacivanje na sredini: 27583 ms

 Pristup podatku: 1179 ms

 Brisanje pocetka: 2491 ms

 Brisanje kraja: 9197 ms

 Brisanje sredine: 10644 ms

 -------------------------------------------

 Segment 5:
 sortiranje liste random generiranih brojeva u rasponu od 1 do 10000 naredbom sort.

 Sortiranje liste: 74341 ms
 Sortiranje liste: 80595 ms
 Sortiranje liste: 58535 ms

Nakon provedenog mjerenja, možemo zaključiti da je Forward_list brza struktura za unos podataka, 
dok je <vector> brzi prilikom brisanja, za dohvat podataka takoder je vektor bio brzi prilikom mjerenja
ali razlika nije bila toliko velika tako da možemo reći da su strukture prilikom dohvata podataka podjednake.

Forward_list-a je dizajnirana za brz upis i brisanje podatka s bilokojeg mjesta u spremniku, dok za razliku od vectora
ne podržava brzi random access podacima.
Vector je sporiji pri unosu na pocetak ili u sredinu, ali na unos na kraju vektor radi brže, ovisno o situaciji
i primjeni ocekuje se da vektor ima bolje performanse od liste zbog nacina na koji je strukturiran i koje 
metode omogucuje.

 
